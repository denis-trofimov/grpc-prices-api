module gitlab.com/denis-trofimov/grpc-prices-api

go 1.14

require (
	github.com/golang/protobuf v1.4.3
	github.com/ilyakaznacheev/cleanenv v1.2.5
	github.com/ory/dockertest/v3 v3.6.3
	github.com/stretchr/testify v1.6.1
	go.mongodb.org/mongo-driver v1.4.4
	golang.org/x/net v0.0.0-20200822124328-c89045814202 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/genproto v0.0.0-20201214200347-8c77b98c765d // indirect
	google.golang.org/grpc v1.34.0
	google.golang.org/protobuf v1.25.0
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
