package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"

	"github.com/ilyakaznacheev/cleanenv"
	pb "gitlab.com/denis-trofimov/grpc-prices-api/internal/proto/prices"
	price_service "gitlab.com/denis-trofimov/grpc-prices-api/internal/service"
	"google.golang.org/grpc"
)

type Config struct {
	Port     string `yaml:"port" env:"PORT" env-default:"9000"`
	Host     string `yaml:"host" env:"HOST" env-default:""`
	Name     string `yaml:"name" env:"NAME" env-default:"grpc-prices-api"`
	MongoURI string `yaml:"mongo_uri" env:"MONGO_URI" env-default:"mongodb://localhost:27017"`
}

var (
	grpcServer *grpc.Server
	srv        *price_service.Server
)

func serve(ctx context.Context) error {
	var cfg Config
	err := cleanenv.ReadEnv(&cfg)
	if err != nil {
		return fmt.Errorf("failed to configure: %w", err)
	}

	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%s", cfg.Host, cfg.Port))
	if err != nil {
		return fmt.Errorf("failed to listen: %w", err)
	}

	srv, err = price_service.New(cfg.MongoURI)
	if err != nil {
		return fmt.Errorf("failed to start DB connection: %w", err)
	}
	grpcServer = grpc.NewServer()

	pb.RegisterPricesServer(grpcServer, srv)

	if err = grpcServer.Serve(lis); err != nil {
		return fmt.Errorf("failed to start grpc Server: %w", err)
	}
	return nil
}

func main() {

	ctx, cancel := context.WithCancel(context.Background())

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)
	defer signal.Stop(interrupt)

	// go func() {
	// 	defer signal.Stop(stop)
	// 	<-stop
	// 	log.Printf("receive interrupt signal")
	// 	cancel()
	// }()

	if err := serve(ctx); err != nil {
		log.Printf("failed to serve:+%s", err)
		cancel()
	} else {
		log.Printf("server started")
	}

	select {
	case <-interrupt:
		log.Printf("receive interrupt signal")
		break
	case <-ctx.Done():
		log.Printf("context closed")
		break
	}

	log.Printf("stopping server by context cancel")

	if grpcServer != nil {
		grpcServer.GracefulStop()
	}
	if srv != nil {
		if err := srv.Disconnect(ctx); err != nil {
			log.Fatalf("DB connection shutdown failed: %s", err)
		}
	}

	log.Printf("server exited properly")
}
