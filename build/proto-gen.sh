#!/bin/sh
# mkdir -p internal/proto/github.com/googleapis/googleapis/google/rpc/status
# wget --output-document=internal/proto/github.com/googleapis/googleapis/google/rpc/status/status.proto \
#     https://github.com/googleapis/googleapis/raw/master/google/rpc/status.proto

# go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
# go get -u github.com/golang/protobuf/protoc-gen-go
# grpc_gateway_path=$(go list -m -f '{{.Dir}}' github.com/grpc-ecosystem/grpc-gateway)
# googleapis_path="$grpc_gateway_path/third_party/googleapis"
# protoc -I $googleapis_path -I internal/proto/prices --go_out=.\
#     --go-grpc_out=. \
#     internal/proto/prices/prices.proto

protoc --go_out=.\
    --go-grpc_out=. \
    internal/proto/prices/prices.proto
