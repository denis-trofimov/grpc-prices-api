package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/denis-trofimov/grpc-prices-api/internal/models"
)

func TestParseCSVContent(t *testing.T) {
	var tests = []struct {
		in   string
		want []models.ProductPrice
	}{
		// note that it is a valid case
		{
			"",
			[]models.ProductPrice{},
		},
		// note that it is a valid case
		{
			"tea;",
			[]models.ProductPrice{
				{Product: "tea"},
			},
		},
		{
			"\n\ntea;78.4\n\n\n\n",
			[]models.ProductPrice{
				{Product: "tea", Price: "78.4"},
			},
		},
		{
			"tea;78.4\ncookie;26.5",
			[]models.ProductPrice{
				{Product: "tea", Price: "78.4"},
				{Product: "cookie", Price: "26.5"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.in, func(t *testing.T) {
			b := []byte(tt.in)
			got, err := parseCSVToProductPrices(&b)
			if err != nil {
				t.Fatal(err)
			}
			for i, want := range tt.want {
				assert.Equal(t, want, (*got)[i])
			}
		})
	}

	var failTests = []string{
		"tea",
		"tea;1;2",
		"tea;78.4\ncookie,26.5",
	}
	for _, tt := range failTests {
		t.Run(tt, func(t *testing.T) {
			b := []byte(tt)
			_, err := parseCSVToProductPrices(&b)
			assert.Error(t, err)
		})
	}
}
