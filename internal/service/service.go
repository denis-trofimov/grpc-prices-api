package service

import (
	"bytes"
	"context"
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"gitlab.com/denis-trofimov/grpc-prices-api/internal/models"
	pb "gitlab.com/denis-trofimov/grpc-prices-api/internal/proto/prices"
	"google.golang.org/grpc/codes"
)

// Server is the server for Prices service.
type Server struct {
	pb.UnimplementedPricesServer
	*models.PriceModel
}

// New creates a new server for dependency injection
func New(mongoURI string) (*Server, error) {
	model, err := models.New(mongoURI)
	if err != nil {
		return nil, err
	}
	return &Server{PriceModel: model}, nil
}

// Fetch a remote CSV file, parse and save to a DB
func (s *Server) Fetch(ctx context.Context, r *pb.FetchRequest) (*pb.FetchResponse, error) {
	body, err := downloadFile(r.Url)
	// TODO: switch on error cases https://cloud.google.com/apis/design/errors#generating_errors
	if err != nil {
		return &pb.FetchResponse{Error: codes.NotFound.String()}, err
	}

	pairs, err := parseCSVToProductPrices(body)
	if err != nil {
		return &pb.FetchResponse{Error: codes.NotFound.String()}, err
	}
	if err := s.PutProductPrices(ctx, pairs); err != nil {
		return &pb.FetchResponse{Error: codes.Internal.String()}, err
	}

	return &pb.FetchResponse{Error: codes.OK.String()}, nil
}

func downloadFile(url string) (*[]byte, error) {
	client := http.Client{
		CheckRedirect: func(r *http.Request, via []*http.Request) error {
			r.URL.Opaque = r.URL.Path
			return nil
		},
	}

	resp, err := client.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return &body, nil
}

// parseCSVToProductPrices parse CSV file contained "PRODUCT NAME;PRICE" to Product and Price pairs
func parseCSVToProductPrices(b *[]byte) (*[]models.ProductPrice, error) {
	r := csv.NewReader(bytes.NewReader(*b))
	r.Comma = ';'
	rows, err := r.ReadAll()
	if err != nil {
		return nil, fmt.Errorf("parseCSVToProductPrices got error %s", err.Error())
	}
	if len(rows) != 0 && len(rows[0]) != 2 {
		return nil, fmt.Errorf("Expected 2 CSV fields in the row, got %d total ", len(rows[0]))
	}
	var productPrices = make([]models.ProductPrice, len(rows))
	// TODO check for decimal in PRICE
	for i, row := range rows {
		if _, err := strconv.Atoi(row[1]); err != nil {
			productPrices[i] = models.ProductPrice{Product: row[0], Price: row[1]}
		} else {
			return nil, err
		}
	}
	return &productPrices, nil
}

// List products loaded from a DB together with prices, updates, update times
func (s *Server) List(ctx context.Context, req *pb.ListRequest) (*pb.ListResponse, error) {
	resp, err := s.ListPrices(ctx, req)
	if err != nil {
		return resp, err
	}
	return resp, nil
}
