package models

import (
	"fmt"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
)

// ParseSortOptions parses a string with ORDER BY SQL syntax to a bson ordered map for the mongoDB driver
// ref: https://docs.mongodb.com/manual/tutorial/sort-results-with-indexes/index.html
// ref: https://cloud.google.com/apis/design/design_patterns#sorting_order
// Sorting Order
// If an API method lets client specify sorting order for list results, the request message should contain a field:
// string order_by = ...;
// The string value should follow SQL syntax: comma separated list of fields. For example: "foo,bar". The default sorting order is ascending. To specify descending order for a field, a suffix " desc" should be appended to the field name. For example: "foo desc,bar".
// Redundant space characters in the syntax are insignificant. "foo,bar desc" and "  foo ,  bar  desc  " are equivalent.
func ParseSortOptions(orderBy string) (sort bson.D, err error) {
	elements := make([]bson.E, 0)
	fields := strings.Split(orderBy, ",")
	for _, fieldOrder := range fields {
		fieldOrder = strings.TrimSpace(fieldOrder)
		fields := strings.Fields(fieldOrder)
		switch len(fields) {
		case 0:
			continue
		case 1:
			elements = append(elements, bson.E{fieldOrder, 1})
		case 2:
			switch strings.ToLower(fields[1]) {
			case "desc":
				elements = append(elements, bson.E{fields[0], -1})
			case "asc":
				elements = append(elements, bson.E{fields[0], 1})
			default:
				return nil, fmt.Errorf("Invalid field order token %s in order_by", fieldOrder)
			}
		default:
			return nil, fmt.Errorf("Invalid field order token %s in order_by", fieldOrder)
		}
	}
	sort = bson.D(elements)
	return
}
