package models

import (
	"context"
	"log"
	"time"

	pb "gitlab.com/denis-trofimov/grpc-prices-api/internal/proto/prices"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"google.golang.org/grpc/codes"
)

// PriceRecord contains product price with updates count and time
type PriceRecord struct {
	ID        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Product   string             `protobuf:"bytes,1,opt,name=product,proto3" json:"product,omitempty" bson:"product,omitempty"`
	Price     string             `protobuf:"bytes,2,opt,name=price,proto3" json:"price,omitempty" bson:"price,omitempty"`
	Updates   int32              `protobuf:"varint,3,opt,name=updates,proto3" json:"updates,omitempty" bson:"updates,omitempty"`
	UpdatedAt time.Time          `protobuf:"bytes,4,opt,name=updated_at,json=updateTime,proto3" json:"updated_at,omitempty" bson:"updated_at,omitempty"`
}

// ProductPrice contains product price
type ProductPrice struct {
	Product, Price string
}

// type PriceRepository interface {
// 	PutProductPrices(context.Context, *[]ProductPrice) error
// 	ListPrices(context.Context, *pb.ListRequest) (*pb.ListResponse, error)
// }

// PriceModel contains storage instance
type PriceModel struct {
	client *mongo.Client
}

// New Create a Client to a MongoDB server and use Ping to verify that the server is running.
func New(uri string) (*PriceModel, error) {
	// Create a Client to a MongoDB server and use Ping to verify that the server is running.
	clientOpts := options.Client().ApplyURI(uri)
	client, err := mongo.Connect(context.TODO(), clientOpts)
	if err != nil {
		log.Print(err)
		return nil, err
	}

	// Call Ping to verify that the deployment is up and the Client was configured successfully.
	// As mentioned in the Ping documentation, this reduces application resiliency as the server may be
	// temporarily unavailable when Ping is called.
	if err = client.Ping(context.TODO(), readpref.Primary()); err != nil {
		log.Print(err)
		return nil, err
	}
	return &PriceModel{client: client}, nil
}

func (m *PriceModel) Disconnect(ctx context.Context) error {
	return m.client.Disconnect(ctx)
}

// PutProductPrices save each product price from a list to the storage
func (m *PriceModel) PutProductPrices(ctx context.Context, prices *[]ProductPrice) error {
	var record PriceRecord
	coll := m.client.Database("prices").Collection("prices")
	opts := options.FindOneAndUpdate().SetUpsert(true)
	for _, price := range *prices {
		filter := bson.M{"product": price.Product}
		update := bson.M{
			"$set": bson.M{"product": price.Product, "price": price.Price, "updated_at": time.Now()},
			"$inc": bson.M{"updates": 1},
		}
		if err := coll.FindOneAndUpdate(ctx, filter, update, opts).Decode(&record); err != nil {
			// ErrNoDocuments means that the filter did not match any documents in the collection
			if err != mongo.ErrNoDocuments {
				return err
			}
		}
	}
	return nil
}

// ListPrices from the storage
func (m *PriceModel) ListPrices(ctx context.Context, req *pb.ListRequest) (*pb.ListResponse, error) {
	coll := m.client.Database("prices").Collection("prices")
	// find all documents, Suppress _id Field
	projection := bson.D{
		{"_id", 0},
		{"product", 1},
		{"price", 1},
		{"updates", 1},
		{"updated_at", 1},
	}
	sort, err := ParseSortOptions(req.GetOrderBy())
	if err != nil {
		return &pb.ListResponse{Error: codes.InvalidArgument.String()}, err
	}
	opts := options.Find().SetProjection(projection).SetSort(sort)
	cursor, err := coll.Find(context.TODO(), bson.M{}, opts)
	if err != nil {
		return &pb.ListResponse{Error: codes.Internal.String()}, err
	}

	var results []pb.PriceRecord
	if err = cursor.All(context.TODO(), &results); err != nil {
		return &pb.ListResponse{Error: codes.Internal.String()}, err
	}
	arrOfPointers := make([]*pb.PriceRecord, len(results))
	for i := range results {
		arrOfPointers[i] = &(results[i])
	}

	return &pb.ListResponse{PriceRecords: arrOfPointers, Error: codes.OK.String()}, nil
}
