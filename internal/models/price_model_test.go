package models

import (
	"context"
	"fmt"
	"log"
	"testing"
	"time"

	"github.com/ory/dockertest/v3"
	"github.com/stretchr/testify/suite"

	pb "gitlab.com/denis-trofimov/grpc-prices-api/internal/proto/prices"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func (m *PriceModel) helperFindByProduct(ctx context.Context, product string) (*PriceRecord, error) {
	var result PriceRecord
	coll := m.client.Database("prices").Collection("prices")
	err := coll.FindOne(context.TODO(), bson.D{{"product", product}}).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

type SortOptionsSuite struct {
	suite.Suite
}
type PricesSuite struct {
	suite.Suite
	client   *mongo.Client
	pool     *dockertest.Pool
	resource *dockertest.Resource
}

// TestRunSuite will be run by the 'go test' command, so within it, we
// can run our suite using the Run(*testing.T, TestingSuite) function.
func TestRunSuite(t *testing.T) {
	suiteTester := new(PricesSuite)
	suite.Run(t, suiteTester)
	suiteTesterSort := new(SortOptionsSuite)
	suite.Run(t, suiteTesterSort)
}

func (s *SortOptionsSuite) TestValidSortOptions() {
	var tests = []struct {
		in   string
		want bson.D
	}{
		{"", bson.D{}},
		// specify the Sort option to sort the returned documents by product in ascending order
		{"product", bson.D{{"product", 1}}},
		// specify the Sort option to sort the returned documents by product in descending order
		{"product desc", bson.D{{"product", -1}}},
		{"price", bson.D{{"price", 1}}},
		{"price desc", bson.D{{"price", -1}}},
		{"updates", bson.D{{"updates", 1}}},
		{"updates desc", bson.D{{"updates", -1}}},
		{"updated_at", bson.D{{"updated_at", 1}}},
		{"updated_at desc", bson.D{{"updated_at", -1}}},
	}
	for _, tt := range tests {
		s.Run(tt.in, func() {
			got, err := ParseSortOptions(tt.in)
			s.Nil(err)
			s.Equal(got, tt.want)
		})
	}
}

func (s *PricesSuite) SetupSuite() {
	// TestMain from https://github.com/ory/dockertest#using-dockertest
	var err error
	// uses a sensible default on windows (tcp/http) and linux/osx (socket)
	s.pool, err = dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	// pulls an image, creates a container based on it and runs it
	s.resource, err = s.pool.Run("mongo", "4.2-bionic", nil)
	if err != nil {
		log.Fatalf("Could not start s.resource: %s", err)
	}

	// exponential backoff-retry, because the application in the container might not be ready to accept connections yet
	if err := s.pool.Retry(func() error {
		var err error
		// not mgo but official driver https://pkg.go.dev/go.mongodb.org/mongo-driver/mongo#example-Connect-Ping
		clientOpts := options.Client().ApplyURI(fmt.Sprintf("mongodb://localhost:%s", s.resource.GetPort("27017/tcp")))
		s.client, err = mongo.Connect(context.TODO(), clientOpts)
		if err != nil {
			return err
		}

		return s.client.Ping(context.TODO(), readpref.Primary())
	}); err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}
}

func (s *PricesSuite) TearDownSuite() {
	// You can't defer this because os.Exit doesn't care for defer
	if err := s.pool.Purge(s.resource); err != nil {
		log.Fatalf("Could not purge s.resource: %s", err)
	}
}

// The TearDownTest method will be run after every test in the suite.
func (s *PricesSuite) TearDownTest() {
	coll := s.client.Database("prices").Collection("prices")
	if _, err := coll.DeleteMany(context.TODO(), bson.M{}); err != nil {
		s.FailNow("cannot delete all prices")
	}
	// if err := coll.Drop(context.TODO()); err != nil {
	// 	s.FailNow("cannot drop collection")
	// }
}

func (s *PricesSuite) TestPutProductPrices() {
	var tests = []struct {
		in   []ProductPrice
		want []PriceRecord
	}{
		{
			[]ProductPrice{
				{Product: "tea", Price: "78.4"},
				{Product: "cookie", Price: "26.5"},
				{Product: "matches", Price: "12.99"},
				{Product: "tea", Price: "67.9"},
				{Product: "cookie", Price: "43.5"},
				{Product: "sugar", Price: "35.99"},
			},
			[]PriceRecord{
				{Product: "tea", Price: "67.9", Updates: 2},
				{Product: "cookie", Price: "43.5", Updates: 2},
				{Product: "matches", Price: "12.99", Updates: 1},
				{Product: "sugar", Price: "35.99", Updates: 1},
			},
		},
	}
	m := PriceModel{client: s.client}
	for _, tt := range tests {
		name := fmt.Sprintf("Product: %v, Price: %v", tt.in[0].Product, tt.in[0].Price)
		s.Run(name, func() {
			if err := m.PutProductPrices(context.TODO(), &tt.in); err != nil {
				s.FailNow(err.Error())
			}
			for _, want := range tt.want {
				got, err := m.helperFindByProduct(context.TODO(), want.Product)
				if err != nil {
					s.FailNow(err.Error())
				}
				s.Equal(want.Price, got.Price)
				s.Equal(want.Updates, got.Updates)
				// price_model_test.go:90: got time 2021-01-08 22:04:20.164 +0000 UTC before want 2021-01-08 22:04:20.164219626 +0000 UTC
				if got.UpdatedAt.Sub(time.Now().UTC()) > 1*time.Second {
					s.Fail("got time %v before want %v", got.UpdatedAt, time.Now().UTC())
				}
			}

		})
	}
}

func (s *PricesSuite) TestSortListPrices() {
	var in = []ProductPrice{
		{Product: "tea", Price: "78.4"},
		{Product: "cookie", Price: "26.5"},
		{Product: "tea", Price: "67.9"},
		{Product: "cookie", Price: "36.5"},
		{Product: "sugar", Price: "35.99"},
		{Product: "cookie", Price: "43.5"},
	}
	var tests = []struct {
		orderBy string
		want    []PriceRecord
		err     string
	}{
		{
			",",
			[]PriceRecord{
				{Product: "tea", Price: "67.9", Updates: 2},
				{Product: "cookie", Price: "43.5", Updates: 3},
				{Product: "sugar", Price: "35.99", Updates: 1},
			},
			"OK",
		},
		{
			" product, ",
			[]PriceRecord{
				{Product: "cookie", Price: "43.5", Updates: 3},
				{Product: "sugar", Price: "35.99", Updates: 1},
				{Product: "tea", Price: "67.9", Updates: 2},
			},
			"OK",
		},
		{
			"  ,  product   desc, ",
			[]PriceRecord{
				{Product: "tea", Price: "67.9", Updates: 2},
				{Product: "sugar", Price: "35.99", Updates: 1},
				{Product: "cookie", Price: "43.5", Updates: 3},
			},
			"OK",
		},
		{
			"price",
			[]PriceRecord{
				{Product: "sugar", Price: "35.99", Updates: 1},
				{Product: "cookie", Price: "43.5", Updates: 3},
				{Product: "tea", Price: "67.9", Updates: 2},
			},
			"OK",
		},
		{
			"price desc",
			[]PriceRecord{
				{Product: "tea", Price: "67.9", Updates: 2},
				{Product: "cookie", Price: "43.5", Updates: 3},
				{Product: "sugar", Price: "35.99", Updates: 1},
			},
			"OK",
		},
		{
			"updates",
			[]PriceRecord{
				{Product: "sugar", Price: "35.99", Updates: 1},
				{Product: "tea", Price: "67.9", Updates: 2},
				{Product: "cookie", Price: "43.5", Updates: 3},
			},
			"OK",
		},
		{
			"updates desc",
			[]PriceRecord{
				{Product: "cookie", Price: "43.5", Updates: 3},
				{Product: "tea", Price: "67.9", Updates: 2},
				{Product: "sugar", Price: "35.99", Updates: 1},
			},
			"OK",
		},
		// test sort on updated_at is fragile
		// {
		// 	"updated_at",
		// 	[]PriceRecord{
		// 		{Product: "tea", Price: "67.9", Updates: 2},
		// 		{Product: "sugar", Price: "35.99", Updates: 1},
		// 		{Product: "cookie", Price: "43.5", Updates: 3},
		// 	},
		// 	"OK",
		// },
		// {
		// 	"update_at desc,",
		// 	[]PriceRecord{
		// 		{Product: "cookie", Price: "43.5", Updates: 3},
		// 		{Product: "sugar", Price: "35.99", Updates: 1},
		// 		{Product: "tea", Price: "67.9", Updates: 2},
		// 	},
		// 	"OK",
		// },

	}
	m := PriceModel{client: s.client}
	if err := m.PutProductPrices(context.TODO(), &in); err != nil {
		s.FailNow(err.Error())
	}
	for _, tt := range tests {
		name := fmt.Sprintf("ListRequest{OrderBy: %s}", tt.orderBy)
		s.Run(name, func() {
			req := pb.ListRequest{
				OrderBy: tt.orderBy,
			}
			resp, err := m.ListPrices(context.TODO(), &req)
			if err != nil {
				s.FailNow(err.Error())
			}
			s.Equal(tt.err, resp.Error)
			for i, got := range resp.PriceRecords {
				s.Equal(tt.want[i].Product, got.Product)
				s.Equal(tt.want[i].Price, got.Price)
				s.Equal(tt.want[i].Updates, got.Updates)
			}
		})
	}
}

func (s *PricesSuite) TestSortCombinationsListPrices() {
	var in = []ProductPrice{
		{Product: "tea", Price: "30"},
		{Product: "cookie", Price: "10"},
		{Product: "sugar", Price: "10"},
		{Product: "cookie", Price: "10"},
	}
	var tests = []struct {
		orderBy string
		want    []PriceRecord
		err     string
	}{
		{
			"price, updates",
			[]PriceRecord{
				{Product: "sugar", Price: "10", Updates: 1},
				{Product: "cookie", Price: "10", Updates: 2},
				{Product: "tea", Price: "30", Updates: 1},
			},
			"OK",
		},
		{
			"price desc, updates",
			[]PriceRecord{
				{Product: "tea", Price: "30", Updates: 1},
				{Product: "sugar", Price: "10", Updates: 1},
				{Product: "cookie", Price: "10", Updates: 2},
			},
			"OK",
		},
		{
			"updates, price desc",
			[]PriceRecord{
				{Product: "tea", Price: "30", Updates: 1},
				{Product: "sugar", Price: "10", Updates: 1},
				{Product: "cookie", Price: "10", Updates: 2},
			},
			"OK",
		},
		{
			"updates desc, price",
			[]PriceRecord{
				{Product: "cookie", Price: "10", Updates: 2},
				{Product: "sugar", Price: "10", Updates: 1},
				{Product: "tea", Price: "30", Updates: 1},
			},
			"OK",
		},
	}
	m := PriceModel{client: s.client}
	if err := m.PutProductPrices(context.TODO(), &in); err != nil {
		s.FailNow(err.Error())
	}
	for _, tt := range tests {
		name := fmt.Sprintf("ListRequest{OrderBy: %s}", tt.orderBy)
		s.Run(name, func() {
			req := pb.ListRequest{
				OrderBy: tt.orderBy,
			}
			resp, err := m.ListPrices(context.TODO(), &req)
			if err != nil {
				s.FailNow(err.Error())
			}
			s.Equal(tt.err, resp.Error)
			for i, got := range resp.PriceRecords {
				s.Equal(tt.want[i].Product, got.Product)
				s.Equal(tt.want[i].Price, got.Price)
				s.Equal(tt.want[i].Updates, got.Updates)
			}
		})
	}
}
